import argparse
import json
import math
import os
import random
import re
import string
import uuid
import xml.etree.ElementTree as ET
import zipfile


random.seed()


# from StackOverflow (plus minor tweaks), claiming it's from Wikipedia, but can't find this anywhere on Wikipedia just now
# https://stackoverflow.com/questions/1181919/python-base-36-encoding
def base36encode(number, alphabet=string.digits+string.ascii_lowercase):
    """Converts an integer to a base36 string."""
    if not isinstance(number, int):
        raise TypeError('number must be an integer')

    base36 = ''
    sign = ''

    if number < 0:
        sign = '-'
        number = -number

    if 0 <= number < len(alphabet):
        return sign + alphabet[number]

    while number != 0:
        number, i = divmod(number, len(alphabet))
        base36 = alphabet[i] + base36

    return sign + base36


# from discussion at https://discordapp.com/channels/170995199584108546/554492873190670336/732451242277732462
def guess_random_foundry_id (length=10):
    id = ''
    while len(id) < length:
        id += base36encode(random.getrandbits(64))
    return id[:length]


ABILITY_MAP = {
    'strength': 'str',
    'dexterity': 'dex',
    'constitution': 'con',
    'intelligence': 'int',
    'wisdom': 'wis',
    'charisma': 'cha',
}

FG_SKILL_PATTERN = re.compile('([a-zA-Z ]+) ([+-][0-9])')
SKILL_MAP = {
    'Acrobatics': 'acr',
    'Animal Handling': 'ani',
    'Arcana': 'arc',
    'Athletics': 'ath',
    'Deception': 'dec',
    'History': 'his',
    'Insight': 'ins',
    'Intimidation': 'itm',
    'Investigation': 'inv',
    'Medicine': 'med',
    'Nature': 'nat',
    'Perception': 'prc',
    'Performance': 'prf',
    'Persuasion': 'per',
    'Religion': 'rel',
    'Sleight of Hand': 'slt',
    'Stealth': 'ste',
    'Survival': 'sur',
}

SIZE_MAP = {
    'Tiny': 'tiny',
    'Small': 'sm',
    'Medium': 'med',
    'Large': 'lg',
    'Huge': 'huge',
    'Gargantuan': 'grg',
}

FVTT_LANGUAGES = (
    'aarakocra',
    'abyssal',
    'aquan',
    'auran',
    'celestial',
    'common',
    'deep',  # Deep Speech
    'draconic',
    'druidic',
    'dwarvish',
    'elvish',
    'giant',
    'gith',
    'gnoll',
    'gnomish',
    'goblin',
    'halfling',
    'ignan',
    'infernal',
    'orc',
    'primordial',
    'sylvan',
    'terran',
    'cant',  # Thieves' Cant
    'undercommon',
)

FVTT_DAMAGE_TYPES = (
    'acid',
    'bludgeoning',
    'cold',
    'fire',
    'force',
    'lightning',
    'necrotic',
    'piercing',
    'poison',
    'psychic',
    'radiant',
    'slashing',
    'thunder'
)

FVTT_CONDITIONS = (
    'blinded',
    'charmed',
    'deafened',
    'diseased',
    'exhaustion',
    'frightened',
    'grappled',
    'incapacitated',
    'invisible',
    'paralyzed',
    'petrified',
    'poisoned',
    'prone',
    'restrained',
    'stunned',
    'unconscious'
)

ATTACK_PATTERN = re.compile(
        '(?P<rangeType>Melee|Ranged) Weapon Attack: (?P<toHit>(\+|-)[0-9]) to hit, (reach (?P<reach>[0-9]+)|range (?P<rangeNormal>[0-9]+)/(?P<rangeFar>[0-9]+)) ft., (?P<target>[^.]+)\. Hit: (?P<damage>[0-9]+) \((?P<damageFormula>[0-9d +-]+)\) (?P<damageType>[a-z]+) damage\.'
)
DAMAGE_BONUS_PATTERN = re.compile('(?P<withoutProf>[0-9d +-]+) (?P<profBonus>(\+|-) [0-9]+)')

#atk1 = 'Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.'
#atk2 = 'Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.'
#bonus = DAMAGE_BONUS_PATTERN.match(ATTACK_PATTERN.match(atk1)['damageFormula'])['profBonus']
#print(bonus); exit(0)

SPELL_CASTING_TIME_PATTERN = re.compile(
    '.* (?P<amount>[0-9]+) (?P<unit>[a-zA-Z]+)')
SPELL_MATERIALS_PATTERN = re.compile(
    '.*M \((?P<materials>[^)]+)\)')
SPELL_RANGE_PATTERN = SPELL_CASTING_TIME_PATTERN
SPELL_RANGE_SHAPE_PATTERN = re.compile(
    '.*: (?P<origin>[a-zA-Z]+) \((?P<amount>[0-9]+)-(?P<unit>[a-zA-Z]+) (?P<shape>[a-zA-Z])\)')

SPELLCASTER_LEVEL_PATTERN = re.compile(
    '.*(?P<level>[0-9]+)[a-z]+[- ]+level spellcaster.*')
SPELLCASTING_ABILITY_PATTERN = re.compile(
    '.*spellcasting ability is (?P<ability>[a-zA-Z]+).*spell save dc (?P<dc>[0-9]+)')

SPELL_ATTACK_PATTERN = re.compile(
    '.*Make a (?P<range>(ranged|melee)) spell attack \((?P<bonus>[+-][0-9]+) to hit\).*')
DAMAGE_FORMULA_PATTERN = re.compile(
    '.*(?P<dice>[0-9]+d[0-9]+) (?P<damageType>[a-z]+) damage.*')
HEALING_FORMULA_PATTERN = re.compile(
    '.*hit points equal to (?P<dice>[0-9]+d[0-9]+)(?P<plus> \+ your spellcasting)?.*')
SAVING_THROW_PATTERN = re.compile(
    '.*DC (?P<dc>[0-9]+) (?P<ability>[a-zA-Z]+) saving throw.*')
CANTRIP_SCALING_PATTERN = re.compile(
    '.*d.?a.?m.?a.?g.?e.? .?i.?n.?c.?r.?e.?a.?s.?e.?s.? .?b.?y.? .?(?P<dice>[0-9]+d[0-9]+) w.?h.?e.?n.? .?y.?o.?u.? .?r.?e.?a.?c.?h.? .?5.?t.?h.? .?l.?e.?v.?e.?l.?.*')
SPELL_SCALING_PATTERN = re.compile(
    '.*s.?p.?e.?l.?l.? .?s.?l.?o.?t.? .?o.?f.? .?[0-9 ]+[a-z ]+ l.?e.?v.?e.?l.? .?o.?r.? .?h.?i.?g.?h.?e.?r.?,.? .?t.?h.?e.? .?[a-z ]+ i.?n.?c.?r.?e.?a.?s.?e.?s.? .?b.?y.? .?(?P<dice>[0-9]+d[0-9]+) f.?o.?r.? .?e.?a.?c.?h.? .?s.?l.?o.?t.*')

# Sacred Flame - Cantrip (At will)
FG_VERBOSE_CANTRIP_NAME_PATTERN = re.compile(
    '(?P<name>.*) -.? .?C.?a.?n.?t.?r.?i.?p.? .?\(.?A.?t.? .?w.?i.?l.?l.?\)')
# Spirit Guardians - 3rd level (2 slots)
FG_VERBOSE_SPELL_NAME_PATTERN = re.compile(
    '(?P<name>.*) -.? .?[0-9]+[a-z]+ .?l.?e.?v.?e.?l.? .?\(.?(?P<slots>[0-9]+) .?s.?l.?o.?t.?s?\)')


# FVTT calls them features, FG traits.
def parse_features (module_title, npc, npc_elem):
    features = []
    for trait in npc_elem.findall('traits/*'):
        feature = {
            '_id': guess_random_foundry_id(),
            'name': trait.find('name').text,
            'type': 'feat',
            'data': {
                'description': {
                    'value': trait.find('desc').text,
                    'chat': '',
                    'unidentified': ''
                },
                'source': module_title,
                'activation': {
                    'type': '',
                    'cost': 0,
                    'condition': ''
                },
                'duration': {
                    'value': None,
                    'units': ''
                },
                'target': {
                    'value': None,
                    'units': '',
                    'type': ''
                },
                'range': {
                    'value': None,
                    'long': None,
                    'units': ''
                },
                'uses': {
                    'value': 0,
                    'max': 0,
                    'per': None
                },
                'consume': {
                    'type': '',
                    'target': None,
                    'amount': None
                },
                'ability': None,
                'actionType': '',
                'attackBonus': 0,
                'chatFlavor': '',
                'critical': None,
                'damage': {
                    'parts': [],
                    'versatile': ''
                },
                'formula': '',
                'save': {
                    'ability': '',
                    'dc': None,
                    'scaling': 'spell'
                },
                'requirements': '',
                'recharge': {
                    'value': None,
                    'charged': False
                },
            },
        }
        features.append(feature)

    return features


def parse_actions (module_title, npc, npc_elem):
    actions = []
    for action in npc_elem.findall('actions/*'):
        action = {
            '_id': guess_random_foundry_id(),
            'name': action.find('name').text,
            'type': 'weapon',  # TODO
            'data': {
                'description': {
                    'value': action.find('desc').text,
                    'chat': '',
                    'unidentified': ''
                },
                'source': module_title,
                'quantity': 1,
                'weight': 0,
                'price': None,
                'attuned': False,
                'equipped': True,
                'rarity': 'Common',
                'identified': True,
                'activation': {
                    'type': 'action',
                    'cost': 1,
                    'condition': ''
                },
                'duration': {
                    'value': None,
                    'units': ''
                },
                'target': {
                    'value': None,
                    'units': '',
                    'type': ''
                },
                'range': {
                    'value': 5,  # just a default
                    'long': None,
                    'units': 'ft'
                },
                'uses': {  # TODO
                    'value': 0,
                    'max': 0,
                    'per': ''
                },
                'consume': {
                    'type': '',
                    'target': '',
                    'amount': None
                },
                'ability': 'str',  # TODO
                'actionType': 'mwak',  # melee weapon attack
                'attackBonus': 0,  # TODO
                'chatFlavor': '',
                'critical': None,
                'damage': {  # TODO
                    'parts': [],
                    'versatile': ''
                },
                'formula': '',
                'save': {
                    'ability': '',
                    'dc': None,
                    'scaling': 'spell'
                },
                'weaponType': 'natural',  # TODO
                'properties': {  # TODO
                    'amm': False,
                    'fin': False,
                    'fir': False,
                    'foc': False,
                    'hvy': False,
                    'lgt': False,
                    'lod': False,
                    'rch': False,
                    'rel': False,
                    'ret': False,
                    'spc': False,
                    'thr': False,
                    'two': False,
                    'ver': False
                },
                'proficient': True,
            },
        }

        attack_match = ATTACK_PATTERN.match(action['data']['description']['value'])
        if attack_match is not None:
            action['data']['range']['value'] = attack_match['reach'] or attack_match['rangeNormal']
            action['data']['range']['long'] = attack_match['rangeFar']  # None is desired if not present
            action['data']['damage']['parts'] = [[
                attack_match['damageFormula'],
                attack_match['damageType']
            ]]

            # actionType 'mwak' seems to be a melee weapon attack.  Ranged is 'rwak'.
            if action['data']['range']['long'] is not None:
                action['data']['actionType'] = 'rwak'

            bonus_match = DAMAGE_BONUS_PATTERN.match(attack_match['damageFormula'])
            if bonus_match is not None:
                bonus = eval(bonus_match['profBonus'].replace(' ', ''))
                if bonus != npc['data']['abilities']['str']['mod']:
                    # TODO: Can we find a normal attack action that uses a stat other than strength or dex?  Should really check a few mods for a match.
                    action['data']['ability'] = 'dex'
                # Remove the proficiency part of the formula dice, now that FVTT will handle it has a proper proficiency bonus.
                # We could instead just mark attacks as not-proficient and not bother, but this extra effort should make NPCs a little easier to scale if a user wants to.
                action['data']['damage']['parts'][0][0] = bonus_match['withoutProf'] + ' + @mod'

        # Not a standard weapon attack?
        # (attack_match is None)
        else:
            action['type'] = 'feat'  # (feature)

        actions.append(action)
    return actions


def guess_spellcaster (npc):
    for item in npc['items']:
        match = SPELLCASTER_LEVEL_PATTERN.match(item['data']['description']['value'].lower())
        if match is None:
            continue

        npc['data']['details']['spellLevel'] = int(match['level'])
        return True
    return False


def guess_spellcasting_ability (npc):
    for item in npc['items']:
        match = SPELLCASTING_ABILITY_PATTERN.match(item['data']['description']['value'].lower())
        if match is None:
            continue

        npc['data']['attributes']['spellcasting'] = ABILITY_MAP[match['ability'].lower()]
        npc['data']['attributes']['spelldc'] = int(match['dc'])
        return True
    return False


def spell_guess_attack_type (spell):
    desc = spell['data']['description']['value']
    match = SPELL_ATTACK_PATTERN.match(desc)
    if match is None:
        return False

    if match['range'] == 'melee':
        spell['data']['actionType'] = 'msak'
    else:
        spell['data']['actionType'] = 'rsak'
    return True


def spell_guess_damage_dice (spell, desc):
    match = DAMAGE_FORMULA_PATTERN.match(desc)
    if match is None:
        return False

    spell['data']['damage'] = {
        'parts': [
            [match['dice'], match['damageType']]
        ],
        'versatile': ''
    }
    return True


def spell_guess_healing_dice (spell, desc):
    match = HEALING_FORMULA_PATTERN.match(desc)
    if match is None:
        return False

    spell['data']['damage'] = {
        'parts': [
            [match['dice'], 'healing']
        ],
        'versatile': ''
    }
    if match['plus'] is not None:
        # Uncertain why this is done with two parts, instead of one formula like an attack.
        #spell['data']['damage']['parts'][0][0] += ' + @mod'
        spell['data']['damage']['parts'].append([
            '@mod', 'healing'
            ])
    return True


def spell_guess_save (spell, desc):
    match = SAVING_THROW_PATTERN.match(desc)
    if match is None:
        return False

    spell['data']['save'] = {
        'ability': ABILITY_MAP[match['ability'].lower()],
        'dc': int(match['dc']),
        'scaling': 'spell'  # TODO
    }
    return True


def spell_guess_cantrip_scaling (spell):
    match = CANTRIP_SCALING_PATTERN.match(spell['data']['description']['value'].lower())
    if match is None:
        return False

    spell['data']['scaling'] = {
        'mode': 'cantrip',  # player level scaling
        'formula': match['dice']
    }
    return True


def spell_guess_spell_scaling (spell):
    match = SPELL_SCALING_PATTERN.match(spell['data']['description']['value'].lower())
    if match is None:
        return False

    spell['data']['scaling'] = {
        'mode': 'level',  # spell slot scaling
        'formula': match['dice']
    }
    return True


def parse_spell (module_title, npc, npc_elem, spell_elem):
    spell = {
        '_id': guess_random_foundry_id(),
        'name': spell_elem.find('name').text,
        'type': 'spell',
        'data': {
            'description': {
                'value': spell_elem.find('desc').text,
                'chat': '',
                'unidentified': ''
            },
            'source': module_title,
            'activation': {
                'type': 'action',  # TODO
                'cost': 1,
                'condition': ''
            },
            'duration': {
                'value': None,
                'units': '',
            },
            'target': {
                'value': None,
                'units': '',
                'type': ''
            },
            'range': {
                'value': None,
                'long': None,
                'units': ''
            },
            'uses': {
                'value': 0,
                'max': 0,
                'per': ''
            },
            'ability': '',
            'actionType': 'util',
            'attackBonus': 0,
            # ...
            'level': 0,
            'school': 'ill',  # FG doesn't tell us this
            'components': {
                'value': '',
                'vocal': False,
                'somatic': False,
                'material': False,
                'ritual': False,
                'concentration': False
            },
            'materials': {
                'value': '',
                'consumed': False,
                'cost': 0,
                'supply': 0
            },
            'preparation': {
                'mode': 'prepared',
                'prepared': True
            },
            'scaling': {
                'mode': 'none',
                'formula': ''
            },
        },
    }

    # remove the "At will" bit FG puts at the end of NPC cantrip names
    #if spell['name'].endswith(' (At will)'):
    #    spell['name'] = spell['name'][:-10]

    # split at literal '\n' text
    new_desc = ''
    lines = spell['data']['description']['value'].replace('\\r','\\n').split('\\n')
    for line in lines:
        if line.startswith('Level:'):
            spell['data']['level'] = int(line[7:])
        elif line.startswith('Casting Time:'):
            match = SPELL_CASTING_TIME_PATTERN.match(line)
            assert match is not None, 'Bad casting time: {}'.format(line)
            spell['data']['activation']['cost'] = int(match['amount'])
            spell['data']['activation']['type'] = match['unit']
        elif line.startswith('Components: '):
            for component in line[12:].split(', '):
                if component.strip() == 'V':
                    spell['data']['components']['vocal'] = True
                elif component.strip() == 'S':
                    spell['data']['components']['somatic'] = True
                elif component.strip().startswith('M ('):
                    match = SPELL_MATERIALS_PATTERN.match(line)
                    assert match is not None, 'Bad spell materials: [{}] from line [{}]'.format(component, line)
                    spell['data']['components']['material'] = True
                    spell['data']['materials']['value'] = match['materials']
                    # TODO: consumed, cost
        elif line.startswith('Duration:'):
            if 'concentration' in line.lower():
                spell['data']['components']['concentration'] = True
            if 'instantaneous' in line.lower():
                spell['data']['duration']['units'] = 'inst'
            else:
                match = SPELL_CASTING_TIME_PATTERN.match(line)
                assert match is not None, 'Bad spell duration: [{}]'.format(line)
                spell['data']['duration']['value'] = int(match['amount'])
                spell['data']['duration']['units'] = match['unit']
        elif line.startswith('Range:'):
            if line.lower().endswith('self'):
                spell['data']['range']['units'] = 'self'
                continue
            elif line.lower().endswith('touch'):
                spell['data']['range']['units'] = 'touch'
                continue

            match = SPELL_RANGE_PATTERN.match(line)
            if match:
                spell['data']['range']['value'] = int(match['amount'])
                if match['unit'] == 'feet':
                    spell['data']['range']['units'] = 'ft'
                else:
                    spell['data']['range']['units'] = match['unit']
                continue

            match = SPELL_RANGE_SHAPE_PATTERN.match(line)
            if match:
                spell['data']['target']['value'] = int(match['amount'])
                spell['data']['target']['units'] = 'ft'  # TODO
                spell['data']['target']['type'] = match['shape'].lower()
                spell['data']['range']['units'] = match['origin'].lower()
                continue
            assert 'Bad spell range: [{}]'.format(line)
        else:
            if len(new_desc) > 0:
                new_desc += '\n'  # human nicety for later editing by hand
            new_desc += '<p>{}</p>'.format(line)
    # (end of for-each-line of description)
    spell['data']['description']['value'] = new_desc

    spell_guess_attack_type(spell)
    spell_guess_damage_dice(spell, spell['data']['description']['value'])
    spell_guess_healing_dice(spell, spell['data']['description']['value'])
    spell_guess_save(spell, spell['data']['description']['value'])
    spell_guess_cantrip_scaling(spell)
    spell_guess_spell_scaling(spell)

    # reduce name verbosity
    match = FG_VERBOSE_CANTRIP_NAME_PATTERN.match(spell['name'])
    if match is not None:
        spell['name'] = match['name']
    match = FG_VERBOSE_SPELL_NAME_PATTERN.match(spell['name'])
    if match is not None:
        spell['name'] = match['name']
        # Fix-up number of slots NPC has at spell level.
        # Note things like the deep gnome may not work right with this approach.
        # (there goes npc being read-only)
        # (also if different spells state different slot counts; last one wins)
        slots = int(match['slots'])
        level = spell['data']['level']
        npc['data']['spells']['spell{}'.format(level)] = {
            'value': slots,
            'max': slots,
            'override': None
        }
    return spell


def parse_spells (module_title, npc, npc_elem, spell_elems):
    cantrips = []
    for spell_elem in spell_elems:
        cantrip = parse_spell(module_title, npc, npc_elem, spell_elem)
        cantrips.append(cantrip)
    return cantrips


def import_npc (module_title, npc_elem):
    #print(npc_elem)
    npc = {
        'abilities': {
            'str': {
                'value': int(npc_elem.find('abilities/strength/score').text),
                #'proficient': 0,  # TODO
                #'min': 3,  # TODO
                'mod': int(npc_elem.find('abilities/strength/bonus').text),
                #'save': 0,  # TODO
                #'prof': 0,  # TODO
            },
            'dex': {
                'value': int(npc_elem.find('abilities/dexterity/score').text),
                #'proficient': 0,  # TODO
                #'min': 3,  # TODO
                'mod': int(npc_elem.find('abilities/dexterity/bonus').text),
                #'save': 0,  # TODO
                #'prof': 0,  # TODO
            },
            'con': {
                'value': int(npc_elem.find('abilities/constitution/score').text),
                #'proficient': 0,  # TODO
                #'min': 3,  # TODO
                'mod': int(npc_elem.find('abilities/constitution/bonus').text),
                #'save': 0,  # TODO
                #'prof': 0,  # TODO
            },
            'int': {
                'value': int(npc_elem.find('abilities/intelligence/score').text),
                #'proficient': 0,  # TODO
                #'min': 3,  # TODO
                'mod': int(npc_elem.find('abilities/intelligence/bonus').text),
                #'save': 0,  # TODO
                #'prof': 0,  # TODO
            },
            'wis': {
                'value': int(npc_elem.find('abilities/wisdom/score').text),
                #'proficient': 0,  # TODO
                #'min': 3,  # TODO
                'mod': int(npc_elem.find('abilities/wisdom/bonus').text),
                #'save': 0,  # TODO
                #'prof': 0,  # TODO
            },
            'cha': {
                'value': int(npc_elem.find('abilities/charisma/score').text),
                #'proficient': 0,  # TODO
                #'min': 3,  # TODO
                'mod': int(npc_elem.find('abilities/charisma/bonus').text),
                #'save': 0,  # TODO
                #'prof': 0,  # TODO
            },
            #'prof': {},  # filled in later
        },  # abilities
        'attributes': {
            'ac': {
                'min': 11,  # TODO
                'value': int(npc_elem.find('ac').text)
            },
            'hp': {
                'value': int(npc_elem.find('hp').text),
                'min': 0,
                'max': int(npc_elem.find('hp').text),
                'temp': 0,
                'tempmax': 0,
                'formula': str(npc_elem.find('hd').text[1:-1]),
            },
            'init': {
                'value': 0,  # TODO
                'bonus': 0,  # TODO
                'mod': 0,  # TODO
                'total': 0,  # TODO
                'prof': 0,  # TODO
            },
            'speed': {
                'value': npc_elem.find('speed').text,
                'special': '',  # TODO
            },
            'spellcasting': '',  # TODO
            'spelldc': 10,  # TODO
            'spellLevel': 0,  # TODO
        },  # attributes
        'details': {
            'alignment': npc_elem.find('alignment').text.title(),
            'biography': {
                'value': '',  # TODO
                'public': '',  # TODO
            },
            'race': None,
            'type': npc_elem.find('type').text.title(),
            'environment': '',  # TODO
            'cr': eval(npc_elem.find('cr').text),
            #'spellLevel': 0,  # TODO
            'xp': { 'value': int(npc_elem.find('xp').text) },
            'source': '',  # TODO
            'class': {}  # TODO
        },
        'skills': {
                'acr': { 'mod': 0, 'ability': 'dex', },
                'ani': { 'mod': 0, 'ability': 'wis', },
                'arc': { 'mod': 0, 'ability': 'int', },
                'ath': { 'mod': 0, 'ability': 'str', },
                'dec': { 'mod': 0, 'ability': 'cha', },
                'his': { 'mod': 0, 'ability': 'int', },
                'ins': { 'mod': 0, 'ability': 'wis', },
                'itm': { 'mod': 0, 'ability': 'cha', },
                'inv': { 'mod': 0, 'ability': 'int', },
                'med': { 'mod': 0, 'ability': 'wis', },
                'nat': { 'mod': 0, 'ability': 'int', },
                'prc': { 'mod': 0, 'ability': 'wis', },
                'prf': { 'mod': 0, 'ability': 'cha', },
                'per': { 'mod': 0, 'ability': 'cha', },
                'rel': { 'mod': 0, 'ability': 'int', },
                'slt': { 'mod': 0, 'ability': 'dex', },
                'ste': { 'mod': 0, 'ability': 'dex', },
                'sur': { 'mod': 0, 'ability': 'wis', },
        },  # skills
        'traits': {
            'size': SIZE_MAP[npc_elem.find('size').text],
            'senses': '',  # TODO
            'languages': {
                'value': [],
                'custom': '',  # TODO
            },
            # damage immunities
            'di': { 'value': [], 'custom': '' },
            # damage resistances
            'dr': { 'value': [], 'custom': '' },
            # damage vulnerabilities
            'dv': { 'value': [], 'custom': '' },
            # condition immunities
            'ci': { 'value': [], 'custom': '' },
        },
        'spells': {
            'spell1': { 'value': 0, 'max': 0, 'override': None },
            'spell2': { 'value': 0, 'max': 0, 'override': None },
            'spell3': { 'value': 0, 'max': 0, 'override': None },
            'spell4': { 'value': 0, 'max': 0, 'override': None },
            'spell5': { 'value': 0, 'max': 0, 'override': None },
            'spell6': { 'value': 0, 'max': 0, 'override': None },
            'spell7': { 'value': 0, 'max': 0, 'override': None },
            'spell8': { 'value': 0, 'max': 0, 'override': None },
            'spell9': { 'value': 0, 'max': 0, 'override': None },
            'pact': { 'value': 0, 'max': 0, 'override': None },
        },
    }

    # figure out more ability values
    #for ab in ('str','dex','con','int','wis','cha'):
    #    npc['abilities'][ab]['save'] = npc['abilities'][ab]['mod']

    # calculate proficiency from CR
    if npc['details']['cr'] == 0:
        npc['attributes']['prof'] = 2
    else:
        npc['attributes']['prof'] = int(1 + math.ceil(npc['details']['cr'] / 4))

    # setup skill bonuses
    skills_elem = npc_elem.find('skills')
    if skills_elem is not None:
        skills = npc_elem.find('skills').text.split(', ')
        #print(skills)
        for skill in skills:
            match = FG_SKILL_PATTERN.match(skill)
            #npc['skills'][SKILL_MAP[match.group(1)]]['mod'] = int(eval(match.group(2)))
            fvtt_skill = SKILL_MAP[match.group(1)]
            skill_mod = int(eval(match.group(2)))
            # figure out if it's proficient or expertise
            fvtt_ability = npc['skills'][fvtt_skill]['ability']
            if npc['abilities'][fvtt_ability]['mod'] + npc['attributes']['prof'] * 2 <= skill_mod:
                npc['skills'][fvtt_skill]['value'] = 2
            elif npc['abilities'][fvtt_ability]['mod'] + npc['attributes']['prof'] <= skill_mod:
                npc['skills'][fvtt_skill]['value'] = 1

    # damage immunities
    di = npc_elem.find('damageimmunities')
    if di is not None:
        entries = di.text.split('; ')
        for entry in entries:
            if entry in FVTT_DAMAGE_TYPES:
                npc['traits']['di']['value'].append(entry)
            else:
                if len(npc['traits']['di']['custom']) > 0:
                    npc['traits']['di']['custom'] += ';'
                npc['traits']['di']['custom'] += entry
    # damage resistances
    dr = npc_elem.find('damageresistances')
    if dr is not None:
        entries = dr.text.split('; ')
        for entry in entries:
            if entry in FVTT_DAMAGE_TYPES:
                npc['traits']['dr']['value'].append(entry)
            else:
                if len(npc['traits']['dr']['custom']) > 0:
                    npc['traits']['dr']['custom'] += ';'
                npc['traits']['dr']['custom'] += entry
    # damage vulnerabilities
    dv = npc_elem.find('damagevulnerabilities')
    if dv is not None:
        entries = dv.text.split('; ')
        for entry in entries:
            if entry in FVTT_DAMAGE_TYPES:
                npc['traits']['dv']['value'].append(entry)
            else:
                if len(npc['traits']['dv']['custom']) > 0:
                    npc['traits']['dv']['custom'] += ';'
                npc['traits']['dv']['custom'] += entry
    # condition immunities
    ci = npc_elem.find('conditionimmunities')
    if ci is not None:
        entries = ci.text.split(', ')
        for entry in entries:
            if entry in FVTT_CONDITIONS:
                npc['traits']['ci']['value'].append(entry)
            else:
                if len(npc['traits']['ci']['custom']) > 0:
                    npc['traits']['ci']['custom'] += ';'
                npc['traits']['ci']['custom'] += entry
    # TODO: Split out the custom ones.

    # fix for custom languages
    fixed_languages = {
        'value': [],
        'custom': ''  # foo;with spaces;bar
    }

    for lang in npc_elem.find('languages').text.split(', '):
        if lang.lower() in FVTT_LANGUAGES:
            fixed_languages['value'].append(lang.lower())
        else:
            if len(fixed_languages['custom']) > 0:
                fixed_languages['custom'] += ';'
            fixed_languages['custom'] += lang
    npc['traits']['languages'] = fixed_languages

    npc = {
        '_id': guess_random_foundry_id(),
        'name': npc_elem.find('name').text,
        'data': npc,
        'type': 'npc',
        'items': [],
    }

    # passed-in npc is meant to be read-only
    npc['items'].extend(parse_actions(module_title, npc, npc_elem))
    npc['items'].extend(parse_features(module_title, npc, npc_elem))

    guess_spellcaster(npc)
    guess_spellcasting_ability(npc)
    npc['items'].extend(parse_spells(module_title, npc, npc_elem, npc_elem.findall('innatespells/*')))
    npc['items'].extend(parse_spells(module_title, npc, npc_elem, npc_elem.findall('spells/*')))


    return npc


def import_npcs (args, module_title, npcs_elem):
    npcs = []
    for category in npcs_elem.iter('category'):
        for npc in category:
            npc_dict = import_npc(module_title, npc)
            npcs.append(npc_dict)

    if len(npcs) <= 0:
        return False

    npc_db_path = os.path.join(args.outdir, 'packs', 'npcs.db')
    with open(npc_db_path, 'wt') as f:
        for npc in npcs:
            f.write(json.dumps(npc) + '\n')

    #print(npcs[-1])
    return os.path.exists(npc_db_path)


def main (args):
    # ensure necessary paths exist
    required_paths = [
            args.outdir,
            os.path.join(args.outdir, 'assets'),
            os.path.join(args.outdir, 'packs'),
            ]
    for path in required_paths:
        if not os.path.exists(path):
            os.makedirs(path)

    # get unix basename-like "leaf" element of path
    # Python's "basename" is less useful for this particular purpose.
    module_name = args.outdir.strip('/\\.').split('/')[-1]
    basename_win = args.outdir.strip('/\\.').split('\\')[-1]
    if len(basename_win) < len(module_name):
        module_name = basename_win
    module = {
        'name': module_name,
        'description': 'Converted Fantasy Grounds module using fg2fvtt-py.  Tool can be found at https://gitlab.com/Akaito1/fg2fvtt-py',
        'version': '0.1.0',
        'minimumCoreVersion': '0.6.5',
        'packs': [],
    }

    with open(args.modfile, 'rb') as f:
        zip = zipfile.ZipFile(f)

        # get the module's name and author(s)
        with zip.open('definition.xml') as def_file:
            root = ET.parse(def_file).getroot()
            module['title'] = root.find('name').text
            module['author'] = root.find('author').text
            assert root.find('ruleset').text == '5E', 'Unrecognized ruleset "{}".  Expected "5E".'.format(root.find('ruleset').text)

        # handle module contents database
        with zip.open('db.xml') as db_file:
            root = ET.parse(db_file).getroot()
            if import_npcs(args, module['title'], root.find('npc')):
                module['packs'].append({
                    'name': 'npcs',
                    'label': 'NPCs from {}'.format(module['title']),
                    'system': 'dnd5e',
                    'path': './packs/npcs.db',
                    'entity': 'Actor',
                })

        # dump assets
        for filename in zip.namelist():
            for extension in ('.jpg', '.jpeg', '.png'):
                if filename.lower().endswith(extension):
                    with zip.open(filename, 'r') as asset:
                        dest = os.path.join(args.outdir, 'assets', filename)
                        with open(dest, 'wb') as asset_out:
                            asset_out.write(asset.read())

    # write out module file
    with open(os.path.join(args.outdir, 'module.json'), 'wt') as f:
        json.dump(module, f,
                indent=4,
                separators=(', ', ': '))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('modfile', help='Path to Fantasy Grounds .mod file')
    parser.add_argument('outdir', help='Where to write the FoundryVTT module')

    args = parser.parse_args()
    main(args)

