fg2fvtt-py
==========

Convert .mod files for Fantasy Grounds into module folders for FoundryVTT.

Example usage:
`$ python main.py ./DDIA-VOLO_-_In_Volos_Wake.mod /mnt/c/Users/akait/AppData/Local/FoundryVTT/Data/modules/in-volos-wake`

This example is run in Windows Subsystem for Linux, but the Python script should work with any OS/shell.
This will take the .mod file specified and output a FVTT module (module.json, packs/npcs.db, etc.) to a place where you can then just enable the module for your game/world in FVTT, and start pulling in NPCs.


TODO
----

- [x] module.json
- [x] actor db
  - [x] Basic name and stats for NPCs.
  - [x] NPC attacks (the standard-language ones).
  - [x] NPC spells.
- [x] dump images into assets folder
- [ ] item db
